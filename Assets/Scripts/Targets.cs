﻿using UnityEngine;
using System.Collections;

public class Targets : MonoBehaviour {

	public GameObject ghost;
	
    //ghosts target a specific tile
	// Update is called once per frame
	void Update () 
	{
		if(ghost.GetComponent<AI>().targetTile != null)
		{
			Vector3 pos = new Vector3(ghost.GetComponent<AI>().targetTile.x, 
										ghost.GetComponent<AI>().targetTile.y, 0f);
			transform.position = pos;
		}
	}
}
