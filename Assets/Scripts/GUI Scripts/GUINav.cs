﻿using System;
using UnityEngine;
using System.Collections;
using System.Text.RegularExpressions;
using UnityEngine.UI;

public class GUINav : MonoBehaviour {

	

	public float initialDelay;
	public Canvas ReadyCanvas;
	public Canvas ScoreCanvas;




    // Use this for initialization
    void Start () 
	{
		StartCoroutine("DisplayScreen", initialDelay);
	}
	
	

	// public handle to show ready screen coroutine call
	public void showScreen()
	{
		StartCoroutine("DisplayScreen", initialDelay);
	}

    //Enumerator to start the level
	IEnumerator DisplayScreen(float seconds)
	{
		//initialWaitOver = false;
		GameManager.gameState = GameManager.GameState.Init;
		ReadyCanvas.enabled = true;
		yield return new WaitForSeconds(seconds);
		ReadyCanvas.enabled = false;
		GameManager.gameState = GameManager.GameState.Game;
		//initialWaitOver = true;
	}



    //Load the game scene
    public void LoadLevel()
    {
        Application.LoadLevel("game");
    }

}
