﻿using UnityEngine;
using System.Collections;

public class Energizer : MonoBehaviour {

    private GameManager gameManager;

    public AudioSource energizerSoundEffect;

	// Use this for initialization
	void Start ()
	{
	    gameManager = GameObject.Find("Game Manager").GetComponent<GameManager>();
	}
    //If pacman collects the energizer powerup
    void OnTriggerEnter2D(Collider2D col)
    {
        if(col.name == "pacman")
        {
            gameManager.ScareGhosts();
            energizerSoundEffect.Play();
            Destroy(gameObject);
        }
    }
}
