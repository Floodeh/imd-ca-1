﻿using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    //--------------------------------------------------------
    // Game variables

    public static int Level = 0;
    public static int lives = 3;

	public enum GameState { Init, Game, Dead, Scores }
	public static GameState gameState;

    private GameObject pacman;
    private GameObject blinky;
    private GameObject pinky;
    private GameObject inky;
    private GameObject clyde;
    private GUINav gui;

	public static bool scared;
    static public int score;

	public float scareLength;
	private float calmTime;

    public float SpeedPerLevel;

    public AudioSource deathSoundEffect;

    //-------------------------------------------------------------------
    // singleton implementation
    private static GameManager instanceNew;

    public static GameManager instance
    {
        get
        {
            if (instanceNew == null)
            {
                instanceNew = GameObject.FindObjectOfType<GameManager>();
                DontDestroyOnLoad(instanceNew.gameObject);
            }

            return instanceNew;
        }
    }

    //-------------------------------------------------------------------
    // function definitions

    void Awake()
    {
        if (instanceNew == null)
        {
            instanceNew = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            if(this != instanceNew)   
                Destroy(this.gameObject);
        }

        AssignGhosts();
    }

    //resets ghosts and the players ghost kills
    private void ResetVariables()
    {
        calmTime = 0.0f;
        scared = false;
        PlayerController.killstreak = 0;
    }

    //Update is called once per frame

    void Update()
    {
        if (scared && calmTime <= Time.time)
            CalmGhosts();

    }

    public void ResetScene()
	{
        CalmGhosts();

        //Resetting the ghosts positions to the centre of the maze
		pacman.transform.position = new Vector3(15f, 11f, 0f);
		blinky.transform.position = new Vector3(15f, 20f, 0f);
		pinky.transform.position = new Vector3(14.5f, 17f, 0f);
		inky.transform.position = new Vector3(16.5f, 17f, 0f);
		clyde.transform.position = new Vector3(12.5f, 17f, 0f);

        //Initializing each ghost and pacman
		pacman.GetComponent<PlayerController>().ResetDestination();
		blinky.GetComponent<GhostMove>().InitializeGhost();
		pinky.GetComponent<GhostMove>().InitializeGhost();
		inky.GetComponent<GhostMove>().InitializeGhost();
		clyde.GetComponent<GhostMove>().InitializeGhost();

        //Calling the ready screen for the start of the level
        gameState = GameState.Init;  
        gui.showScreen();
        
	}

    //Scare the ghosts if they are not scared already and if they are calm them
	public void ToggleScare()
	{
		if(!scared)	ScareGhosts();
		else 		CalmGhosts();
	}

    //Ghosts are scared once an energizer powerup is collected
    //This method scares the ghosts and makes them run from pacman
    //by calling the frighten method
	public void ScareGhosts()
	{
		scared = true;
		blinky.GetComponent<GhostMove>().Frighten();
		pinky.GetComponent<GhostMove>().Frighten();
		inky.GetComponent<GhostMove>().Frighten();
		clyde.GetComponent<GhostMove>().Frighten();
		calmTime = Time.time + scareLength;

        //we used this log for testing to see if the method was being called
        Debug.Log("Ghosts Scared");
	}

    //After the timer runs out for the energizer the ghosts are calmed
    //This sets the ghosts ai back to the original format
    //and zone in towards pacman rather than running away
	public void CalmGhosts()
	{
		scared = false;
		blinky.GetComponent<GhostMove>().Calm();
		pinky.GetComponent<GhostMove>().Calm();
		inky.GetComponent<GhostMove>().Calm();
		clyde.GetComponent<GhostMove>().Calm();
	    PlayerController.killstreak = 0;
    }

    //Assigning ghosts to their game objects
    void AssignGhosts()
    {
        // find and assign ghosts
        clyde = GameObject.Find("clyde");
        pinky = GameObject.Find("pinky");
        inky = GameObject.Find("inky");
        blinky = GameObject.Find("blinky");
        pacman = GameObject.Find("pacman");

        if (clyde == null || pinky == null || inky == null || blinky == null) Debug.Log("One of ghosts are NULL");
        if (pacman == null) Debug.Log("Pacman is NULL");

        gui = GameObject.FindObjectOfType<GUINav>();

        if(gui == null) Debug.Log("GUI Handle Null!");

    }

    //When pacman is hit by a ghost that is not frightened kill pacman
    //subtract a life and play the dead animation and sound effect
    //also update the UI with the lives counter
    public void LoseLife()
    {
        lives--;
        gameState = GameState.Dead;
        deathSoundEffect.Play();
    
        // update UI too
        UI ui = GameObject.FindObjectOfType<UI>();
        Destroy(ui.lives[ui.lives.Count - 1]);
        ui.lives.RemoveAt(ui.lives.Count - 1);
    }

    //public static void DestroySelf()
    //{
    //    score = 0;
    //    Level = 0;
    //    lives = 3;
    //    Destroy(GameObject.Find("Game Manager"));
    //}
}
