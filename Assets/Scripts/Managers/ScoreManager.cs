﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class ScoreManager : MonoBehaviour {

    public class Score
    {
        //Getter and setters for score and initializing score as incoming variable "s"
        public int score { get; set; }

        public Score(int s)
        {
            score = s;
        }
    }
}
