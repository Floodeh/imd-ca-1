﻿using UnityEngine;
using System.Collections;

public class Pacdot : MonoBehaviour {

    //when pacman hits a pacdot
    //collect 10 score and destroy the pacdot.
	void OnTriggerEnter2D(Collider2D other)
	{
		if(other.name == "pacman")
		{
			GameManager.score += 10;
		    GameObject[] pacdots = GameObject.FindGameObjectsWithTag("pacdot");
            Destroy(gameObject);

            //when all of the pacdots are gone reload the level
		    if (pacdots.Length == 1)
		    {
		        GameObject.FindObjectOfType<GUINav>().LoadLevel();
		    }
		}
	}
}
