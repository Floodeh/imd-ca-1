﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI : MonoBehaviour {

	public int high, score;

	public List<Image> lives = new List<Image>(3);

    private Text scoreText;
	
	// Use this for initialization
	void Start () 
	{
		scoreText = GetComponentsInChildren<Text>()[1];

	    for (int i = 0; i < 3 - GameManager.lives; i++)
	    {
	        Destroy(lives[lives.Count-1]);
            lives.RemoveAt(lives.Count-1);
	    }
	}
	
	// Update is called once per frame
	void Update () 
	{

        // update score text
        score = GameManager.score;
		scoreText.text = "Score\n" + score;
	}


}
